# **KnightCMS** Themes - _Sablonkezelő_

**What is this?**

This package is used my own CMS for Frontend Themes.
**KnightCMS isn't public,  please use original package: [voyager-themes](https://github.com/thedevdojo/voyager-themes)**

*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --*

**Mi ez?**

A KnightCMS rendszer a [voyager-themes](https://github.com/thedevdojo/voyager-themes) csomag továbbfejlesztett verzióját használja, a témakezeléshez.

Maga a CMS rendszer jelenleg nem publikus, így ha nem rendelkezel vele, akkor vedd fel velünk a kapcsolatot, vagy használt a _voyager-themes_ csomagot.

**Szükséges csomagok**
- Laravel 6.0
- [Voyager v1.3 Admin csomag](https://github.com/the-control-group/voyager)

## Telepítés

```bash
composer require knightcms/themes
```