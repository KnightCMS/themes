<?php

namespace Knightcms\Themes\Models;

use Illuminate\Database\Eloquent\Model;

class ThemeOptions extends Model
{
	protected $table = 'knightcms_theme_options';
    protected $fillable = ['knightcms_theme_id', 'key', 'value'];
}
